//menu button
const menu = document.getElementById('menuButton')
menu.addEventListener("click" , function(){
    buttonFunction(this)})

function buttonFunction(x)
{
    x.classList.toggle("change")
}
//calendar
document.addEventListener('DOMContentLoaded', function() {
    var today = new Date();
    var tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    var tomorrowFormatted = tomorrow.toISOString().split('T')[0];
    document.getElementById('date').min = tomorrowFormatted;
});

//student rate
const strate = document.getElementById('strate')
const stucount = document.getElementById('students')

let scount = Number(stucount.value)
let snew = 0

function updateRate() {
    snew = scount * 150
    strate.innerHTML = snew
}

stucount.addEventListener("input", function() {
    scount = Number(stucount.value)
    updateRate()
});

updateRate();

//family rate
 document.addEventListener("DOMContentLoaded", function() {
    const adult = document.getElementById('adults')
    const child = document.getElementById('children')
    const famprice = document.getElementById('famprice')
//console.log(famprice)
    function famupdate() {
         const adultcount = Number(adult.value);
        const childcount = Number(child.value);
        const adultprice = adultcount * 180;
        const childprice = childcount * 90;
        const newfam = adultprice + childprice;
        famprice.innerHTML = newfam;
    }

    adult.addEventListener("input", famupdate)
    child.addEventListener("input", famupdate)

    famupdate()
});   

//friends update
const frndnum =document.getElementById('adulnos')
const frnprice = document.getElementById( 'frnprice' )

let fprice = 0
function frnfunction(){
    const fcount = Number(frndnum.value)
    fprice=fcount*200
    frnprice.innerHTML = fprice
}

frndnum.addEventListener('change',frnfunction)
frnfunction()


